﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExemplesCSharp
{
    class DemoReference
    {


        public void Tester()
        {
            int x;
            bool ok = false;
            do
            {
                Console.WriteLine("Veuillez saisir un nombre : ");
                string s = Console.ReadLine();
                //x = int.Parse(s);
                ok = int.TryParse(s, out x);
            }
            while (!ok);

            int y = 42;
            bool resultat = Doubler(ref x);

            int p;
            Attribuer(out p);

            Console.WriteLine("Le double de x vaut : " + x);
        }

        public bool MonTryParse(string s, out int result)
        {
            bool success = true;
            try
            {
                result = int.Parse(s);
            }
            catch(FormatException exc)
            {
                result = 0;
                success = false;
            }
            return success;
        }

        public void Attribuer(out int x)
        {
            x = 42;
        }

        public bool Doubler(ref int x)
        {
            x = x * 2;
            return x < 100;
        }

    }


}
